//
//  GameManager.h
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#ifndef __LD__GameManager__
#define __LD__GameManager__

#include "cocos2d.h"

class GameManager
{
private:
    GameManager();
    static GameManager* m_singleton;
public:
    int currentLevel;
    
    static GameManager* sharedGameManager();
    
    void restartLevel();
    void nextLevel();
    
    void launchMainMenu();
    void launchSelectLevel();
};
#endif /* defined(__LD__GameManager__) */
