//
//  Wall.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#include "Wall.h"
#include "Constants.h"
#include "MapHelper.h"

Wall::Wall()
{
    
}

Wall::~Wall()
{
    delete points;
}

bool Wall::init(b2World *world, CCDictionary *properties)
{
    points = MapHelper::parseDictionary(properties);
    
    
    if (CCNode::init())
    {
        //setTag(ObjectTypePlatform);
        b2BodyDef bodyDef;
        bodyDef.type = b2_staticBody;
        bodyDef.position.Set(0, 0);
        body = world->CreateBody(&bodyDef);
        
        b2EdgeShape groundBox;
        for (int i = 0; i < points->count()-1; i++)
        {
            CCPoint pos = points->getControlPointAtIndex(i);
            CCPoint nextPos = points->getControlPointAtIndex(i+1);
            groundBox.Set(b2Vec2(pos.x/PTM_RATIO,pos.y/PTM_RATIO), b2Vec2(nextPos.x/PTM_RATIO,nextPos.y/PTM_RATIO));
            body->CreateFixture(&groundBox,0);
            
//            b2Fixture *fixture = body->GetFixtureList();
//            b2Filter filter;
//            filter.categoryBits = CATEGORY_PLATFORM;
//            filter.maskBits = MASK_PLATFORM;
//            fixture->SetFilterData(filter);
        }
       
        body->SetUserData(this);
        return true;
    }
    return false;
}