//
//  ChangeLevel.h
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#ifndef __LD__ChangeLevel__
#define __LD__ChangeLevel__

#include "cocos2d.h"
USING_NS_CC;

class ChangeLevel : public CCLayer {
public:
    static CCScene* scene();
    
    ~ChangeLevel();
    ChangeLevel();
    void play(CCMenuItem *item);

};
#endif /* defined(__LD__ChangeLevel__) */
