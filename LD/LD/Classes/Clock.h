//
//  Clock.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__Clock__
#define __LD__Clock__

#include "cocos2d.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class Clock : public CCSprite
{
    
public:
    ~Clock();
    Clock();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
};
#endif /* defined(__LD__Clock__) */
