//
//  Constants.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef LD_Constants_h
#define LD_Constants_h

#define PLAYER_SIZE 12.5 //100/2
#define PTM_RATIO 32
#define PLAYER_SPEED 7
#define ENEMY_SPEED 8
#define START_PLAYER_TIME 10
#define FRIZE_TIME 5.0

enum _entityCategory {
    CATEGORY_PLAYER =          0x0001,
    CATEGORY_ENEMY =       0x0002,
    CATEGORY_WALL =     0x0004,
    CATEGORY_BULLET =      0x0008,
};

enum _entityMask {
    MASK_BULLET = CATEGORY_PLAYER | CATEGORY_WALL,
    MASK_ALL = CATEGORY_PLAYER | CATEGORY_WALL |CATEGORY_ENEMY | CATEGORY_BULLET
};

typedef enum {
    ObjectTypePlayer,
    ObjectTypePlatform,

    ObjectTypeClock,
    ObjectTypeSnowBonus,
    ObjectTypeEnemy,
    ObjectTypeBullet,
    ObjectTypeFinishLine,
    NeedDestroyBody,
} ObjectType;
#endif
