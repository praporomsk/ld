//
//  Wall.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__Wall__
#define __LD__Wall__

#include "cocos2d.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class Wall : public CCNode
{
public:
    ~Wall();
    Wall();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
    CCPointArray *points;
};

#endif /* defined(__LD__Wall__) */
