//
//  GameManager.cpp
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#include "GameManager.h"
#include "GameManager.h"
#include "GameLayer.h"
#include "ChangeLevel.h"

GameManager *GameManager::m_singleton = NULL;

GameManager::GameManager()
{
    
}

GameManager* GameManager::sharedGameManager()
{
    if (NULL == m_singleton)
    {
        m_singleton = new GameManager();
        m_singleton->currentLevel = 0;
    }
    return  m_singleton;
}

void GameManager::restartLevel()
{
    CCDirector::sharedDirector()->replaceScene(GameLayer::scene());
}

void GameManager::nextLevel()
{
    
}

void GameManager::launchMainMenu()
{
    CCDirector::sharedDirector()->replaceScene(ChangeLevel::scene());
}

void GameManager::launchSelectLevel()
{
    CCDirector::sharedDirector()->replaceScene(GameLayer::scene());
    CCLOG("launchSelectLevel");
}