//
//  Enemy.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#include "Enemy.h"
#include "MapHelper.h"
#include "Constants.h"
#include "Wall.h"
#include "Bullet.h"

#include "SimpleAudioEngine.h"
using namespace CocosDenshion;

Enemy::Enemy()
{
    
}

Enemy::~Enemy()
{
    
}

bool Enemy::init(b2World *world, CCDictionary *properties, int type)
{
    //properties->valueForKey("type")->floatValue();

    CCPoint pos = MapHelper::positionWithProperties(properties);
    CCString *name = CCString::createWithFormat("enemy_%02d.png",type);
    if (CCSprite::initWithSpriteFrameName(name->getCString()))
    {
        m_world = world;
        m_type = type;
        setPosition(pos);
        setTag(ObjectTypeEnemy);
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
        
        body = world->CreateBody(&bodyDef);
        
        //body->SetFixedRotation(true);
        
        // Define another box shape for our dynamic body.
        b2PolygonShape dynamicBox;
        dynamicBox.SetAsBox((boundingBox().size.width/2) / PTM_RATIO,
                            (boundingBox().size.width/2) / PTM_RATIO);//These are mid points for our 1m box
        
        // Define the dynamic body fixture.
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &dynamicBox;
        fixtureDef.density = 10.0f;
        fixtureDef.friction = 0.1f;
        fixtureDef.restitution = 0.0f;
        fixtureDef.filter.categoryBits = CATEGORY_ENEMY;
        fixtureDef.filter.maskBits = MASK_ALL;
        
        body->CreateFixture(&fixtureDef);
        body->SetUserData(this);
        int intTime = arc4random() % 100;
        float time = (float)intTime / 100;
        CCDelayTime *delay = CCDelayTime::create(time);
        CCCallFunc *playUpdate = CCCallFunc::create(this, callfunc_selector(Enemy::playUpdate));
        CCSequence *seq = CCSequence::createWithTwoActions(delay, playUpdate);
        runAction(seq);
        return true;
    }
    return false;
}

void Enemy::playUpdate()
{
    if (m_type == 1)
    {
        this->schedule( schedule_selector(Enemy::updateAI), 2.0f);
    }else{
        this->schedule( schedule_selector(Enemy::updateAI), 1.0f);
    }
    
}

void Enemy::updateAI()
{
    CCPoint direction = ccpSub(gameGelegate->getPlayer()->getPosition(), getPosition());
    if (ccpLength(direction) > 300) return;
    
    direction = ccpNormalize(direction);
    
    if (isSeePlayer())
    {//move to player
        if (m_type == 0){
            
            direction = ccpMult(direction, ENEMY_SPEED);
            body->SetLinearVelocity(b2Vec2(direction.x, direction.y));
        }else{
            if (!gameGelegate->getPlayer()->isFrieze)
            {
                SimpleAudioEngine::sharedEngine()->playEffect("sounds/shoot.wav");
                
                float32 rotatationAngle = ccpToAngle(direction);
                body->SetTransform(body->GetPosition(), rotatationAngle);
                
                Bullet *bullet = new Bullet();
                bullet->init(m_world, getPosition());
                bullet->autorelease();
                getParent()->addChild(bullet);
                
                direction = ccpMult(direction, 10);
                bullet->body->SetLinearVelocity(b2Vec2(direction.x, direction.y));
            }
        }
    }else{
        body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
    }
}

bool Enemy::isSeePlayer()
{
    CCPoint myStartPos = getPosition();
    CCPoint myEndPos = gameGelegate->getPlayer()->getPosition();
    CCArray* walls = gameGelegate->getWalls();
    

    CCObject* obj;
    CCARRAY_FOREACH(walls,obj)
    {
        Wall *wall = (Wall*)obj;
        CCPointArray *points = wall->points;
        //CCLog("points->count =%d",points->count());
        for (int i = 0; i < points->count()-1; i++)
        {
                CCPoint startPoint = points->getControlPointAtIndex(i);
                CCPoint endPoint = points->getControlPointAtIndex(i+1);
                if(intersect(startPoint, endPoint, myStartPos, myEndPos))
                    return false;
        }
    }

    return true;
}

inline int area (CCPoint a, CCPoint b, CCPoint c) {
	return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

inline bool intersect_1 (int a, int b, int c, int d) {
	if (a > b)  swap (a, b);
	if (c > d)  swap (c, d);
	return max(a,c) <= min(b,d);
}

bool Enemy::intersect (CCPoint a, CCPoint b, CCPoint c, CCPoint d) {
	return intersect_1 (a.x, b.x, c.x, d.x)
    && intersect_1 (a.y, b.y, c.y, d.y)
    && area(a,b,c) * area(a,b,d) <= 0
    && area(c,d,a) * area(c,d,b) <= 0;
}