//
//  Player.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#include "Player.h"
#include "MapHelper.h"
#include "Constants.h"
#include "SimpleAudioEngine.h"
#include "GameManager.h"

using namespace CocosDenshion;

Player::Player()
{
    
}

Player::~Player()
{
    
}

bool Player::init(b2World *world, CCDictionary *properties)
{

    CCPoint pos = MapHelper::positionWithProperties(properties);
    if (CCSprite::initWithSpriteFrameName("Player_00.png"))
    {
        m_world = world;
        setPosition(pos);
        setTag(ObjectTypePlayer);
        m_slowdown = false;
        setScale(1.5f);

        isFrieze = false;
        reCreateBody();
        playerParticle = NULL;

        return true;
    }
    return false;
}

void Player::reCreateBody()
{
    CCPoint pos = getPosition();
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    
    body = m_world->CreateBody(&bodyDef);
    
    //body->SetFixedRotation(true);
    
    // Define another box shape for our dynamic body.
    b2PolygonShape dynamicBox;
    dynamicBox.SetAsBox((PLAYER_SIZE * getScale()) / PTM_RATIO, (PLAYER_SIZE * getScale()) / PTM_RATIO) ;//These are mid points for our 1m box
    
    
    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &dynamicBox;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.1f;
    fixtureDef.restitution = 0.2f;
    fixtureDef.filter.categoryBits = CATEGORY_PLAYER;
    fixtureDef.filter.maskBits = MASK_ALL;

    body->CreateFixture(&fixtureDef);
    body->SetUserData(this);
}

void Player::setSlowdown(bool slowdown)
{
    if (m_slowdown == slowdown) return;
    
    m_slowdown = slowdown;
    if (m_slowdown) {
        scheduleUpdate();
    }else{
        unscheduleUpdate();
    }
}

void Player::frize()
{
    if (!isFrieze) {
        isFrieze = true;
        body->SetType(b2_staticBody);
        body->SetBullet(true);
        //body->SetActive(false);
        CCTintTo *tint = CCTintTo::create(1.0, 123, 184, 170);
        CCDelayTime *time = CCDelayTime::create(1.1);
        CCCallFunc *changed = CCCallFunc::create(this, callfunc_selector(Player::unFrize));
        CCSequence *seq = CCSequence::create(tint, time, changed, NULL);
        runAction(seq);
    }
}

void Player::unFrize()
{
    isFrieze = false;
    body->SetType(b2_dynamicBody);
    //body->SetActive(false);
    CCTintTo *tint = CCTintTo::create(0.5, 255, 255, 255);
    runAction(tint);
}

void Player::update(float dt)
{
    float slowdown = 0.95f;
    b2Vec2 velocity = body->GetLinearVelocity();
    float dis = ccpLength(ccp(velocity.x, velocity.y));
    if (dis < 0.1)
        setSlowdown(false);
    
    body->SetLinearVelocity(b2Vec2(velocity.x * slowdown, velocity.y * slowdown));
}

void Player::moveToDirection(CCPoint direction)
{
    direction = ccpMult(direction, PLAYER_SPEED);
    body->SetLinearVelocity(b2Vec2(direction.x,direction.y));
}

void Player::change()
{
    SimpleAudioEngine::sharedEngine()->playEffect("sounds/powerUP.wav");

    float newScale;
    
    int level = GameManager::sharedGameManager()->currentLevel;
    if (level == 1)
    {
        if (getScale() == 1.5)
        {
            newScale = 1.0;
        }else if(getScale() == 1.0){
            newScale = 0.5;
        }else if(getScale() == 0.5){
            newScale = 1.5;
        }
    }else{//level 0
        if (getScale() == 1.5)
        {
            newScale = 1.0;
        }else{
            
            newScale = 1.5;
        }
    }

    playerParticle->setScale(newScale);
    
    CCScaleTo *scale = CCScaleTo::create(1.0, newScale);
    CCEaseElasticOut *ease = CCEaseElasticOut::create(scale, 0.3);
    CCCallFunc *changed = CCCallFunc::create(this, callfunc_selector(Player::changed));
    CCSequence *seq = CCSequence::createWithTwoActions(ease, changed);

    runAction(seq);
}

void Player::changed()
{
    m_world->DestroyBody(body);
    reCreateBody();
}