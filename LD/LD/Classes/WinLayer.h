//
//  WinLayer.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__WinLayer__
#define __LD__WinLayer__

#include "cocos2d.h"
USING_NS_CC;

class WinLayer : public CCLayer {
public:
    static CCScene* scene();
    
    ~WinLayer();
    WinLayer();
    
    void nextLevel();
};
#endif /* defined(__LD__WinLayer__) */
