//
//  Bullet.h
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#ifndef __LD__Bullet__
#define __LD__Bullet__

#include "cocos2d.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class Bullet : public CCSprite
{
    
public:
    ~Bullet();
    Bullet();
    
    bool init(b2World* world, CCPoint pos);
    b2Body *body;
};
#endif /* defined(__LD__Bullet__) */
