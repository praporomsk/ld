//
//  SnowBonus.h
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#ifndef __LD__SnowBonus__
#define __LD__SnowBonus__

#include "cocos2d.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class SnowBonus : public CCSprite
{
    
public:
    ~SnowBonus();
    SnowBonus();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
};
#endif /* defined(__LD__SnowBonus__) */
