//
//  GameDelegate.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef LD_GameDelegate_h
#define LD_GameDelegate_h
#include "cocos2d.h"
#include "Player.h"

class GameDelegate
{
public:
    GameDelegate() {};
    virtual ~GameDelegate() {};
    
    virtual CCArray* getWalls() {};
    virtual Player* getPlayer() {};
};

#endif
