//
//  ChangeLevel.cpp
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#include "ChangeLevel.h"
#include "GameLayer.h"
#include "GameManager.h"
#include "SimpleAudioEngine.h"
using namespace CocosDenshion;


CCScene* ChangeLevel::scene()
{
    CCScene *scene = CCScene::create();
    CCLayer* layer = new ChangeLevel();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}

ChangeLevel::ChangeLevel()
{

    SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sounds/background.mp3", -1);
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

    CCMenuItemSprite *nextItem = CCMenuItemSprite::create(CCSprite::create("level1.png"),
                                                          CCSprite::create("level1Sel.png"),
                                                          this,
                                                          menu_selector(ChangeLevel::play));
//    CCLabelBMFont *nextLevel = CCLabelBMFont::create("level 1", "Fonts/TimeFont.fnt");
//    CCMenuItemLabel *nextItem = CCMenuItemLabel::create(nextLevel,
//                                                        this,
//                                                        menu_selector(ChangeLevel::play));
    nextItem->setPosition(ccp(screenSize.width/2, screenSize.height * 0.65));
    nextItem->setTag(0);

    CCMenuItemSprite *twoItem = CCMenuItemSprite::create(CCSprite::create("level2.png"),
                                                          CCSprite::create("level2Sel.png"),
                                                          this,
                                                          menu_selector(ChangeLevel::play));
    
//    CCLabelBMFont *twoLevel = CCLabelBMFont::create("level 2", "Fonts/TimeFont.fnt");
//    CCMenuItemLabel *twoItem = CCMenuItemLabel::create(twoLevel,
//                                                        this,
//                                                        menu_selector(ChangeLevel::play));
    twoItem->setPosition(ccp(screenSize.width/2, screenSize.height * 0.35));
    twoItem->setTag(1);
    
    CCMenu *menu = CCMenu::create(nextItem, twoItem,NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
}

ChangeLevel::~ChangeLevel()
{
    
}

void ChangeLevel::play(CCMenuItem *item)
{
    GameManager::sharedGameManager()->currentLevel = item->getTag();
    GameManager::sharedGameManager()->launchSelectLevel();
}