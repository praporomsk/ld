//
//  SnowBonus.cpp
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#include "SnowBonus.h"
#include "MapHelper.h"
#include "Constants.h"

SnowBonus::SnowBonus()
{
    
}

SnowBonus::~SnowBonus()
{
    
}

bool SnowBonus::init(b2World *world, CCDictionary *properties)
{
    CCPoint pos = MapHelper::positionWithProperties(properties);
    if (CCSprite::initWithSpriteFrameName("snowBonus.png"))
    {
        setPosition(pos);
        setTag(ObjectTypeSnowBonus);
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
        
        body = world->CreateBody(&bodyDef);
        
        //body->SetFixedRotation(true);
        
        // Define another box shape for our dynamic body.
        b2CircleShape *circleShape = new b2CircleShape();
        circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
        
        // Define the dynamic body fixture.
        b2FixtureDef fixtureDef;
        fixtureDef.shape = circleShape;
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 0.1f;
        fixtureDef.restitution = 0.0f;
        //fixtureDef.filter.categoryBits = CATEGORY_SUN;
        //fixtureDef.filter.maskBits = MASK_SUN;
        
        delete circleShape;
        circleShape = NULL;
        
        body->CreateFixture(&fixtureDef);
        body->SetUserData(this);
        
        return true;
    }
    return false;
}