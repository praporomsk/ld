//
//  WinLayer.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#include "WinLayer.h"
#include "GameLayer.h"
#include "GameManager.h"

CCScene* WinLayer::scene()
{
    CCScene *scene = CCScene::create();
    
    CCLayer* layer = new WinLayer();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}

WinLayer::WinLayer()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    CCLabelBMFont *win = CCLabelBMFont::create("you win", "Fonts/TimeFont.fnt");
    win->setScale(2.0);
    win->setPosition(ccp(screenSize.width/2, screenSize.height * 0.7));
    addChild(win);
    
    CCLabelBMFont *nextLevel = CCLabelBMFont::create("menu", "Fonts/TimeFont.fnt");
    
    CCMenuItemLabel *nextItem = CCMenuItemLabel::create(nextLevel,
                                                        this,
                                                        menu_selector(WinLayer::nextLevel));
    nextItem->setPosition(ccp(screenSize.width/2, screenSize.height/2));
    
    CCMenu *menu = CCMenu::create(nextItem, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
}

WinLayer::~WinLayer()
{
    
}

void WinLayer::nextLevel()
{
    GameManager::sharedGameManager()->launchMainMenu();
    //CCDirector::sharedDirector()->replaceScene(GameLayer::scene());
}