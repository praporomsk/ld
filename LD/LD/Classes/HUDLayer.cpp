//
//  HUDLayer.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#include "HUDLayer.h"
#include "GameManager.h"
HUDLayer::HUDLayer()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    m_time = CCLabelBMFont::create("10", "Fonts/TimeFont.fnt");
    m_time->setPosition(ccp(screenSize.width * 0.1, screenSize.height * 0.9));
    addChild(m_time);
    
    CCSprite *exitNorm = CCSprite::create("exit.png");
    CCSprite *exitSel = CCSprite::create("exitSel.png");
    
    CCMenuItemSprite *exitItem = CCMenuItemSprite::create(exitNorm,
                                                          exitSel,
                                                          this,
                                                          menu_selector(HUDLayer::exit));
    exitItem->setPosition(screenSize.width * 0.9, screenSize.height * 0.9);
    
    CCMenu *menu = CCMenu::create(exitItem, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
    
}

HUDLayer::~HUDLayer()
{
    
}

void HUDLayer::exit()
{
    GameManager::sharedGameManager()->launchMainMenu();
}

void HUDLayer::updateTime(float time)
{
    int intTime = (int)time;
    m_time->setString(CCString::createWithFormat("%d",intTime)->getCString());
}