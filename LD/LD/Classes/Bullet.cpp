//
//  Bullet.cpp
//  LD
//
//  Created by Roman on 8/25/13.
//
//

#include "Bullet.h"
#include "MapHelper.h"
#include "Constants.h"

Bullet::Bullet()
{
    
}

Bullet::~Bullet()
{
    
}

bool Bullet::init(b2World *world, CCPoint pos)
{
    
    if (CCSprite::initWithSpriteFrameName("showBullet.png"))
    {
        setPosition(pos);
        setTag(ObjectTypeBullet);
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
        
        body = world->CreateBody(&bodyDef);
        body->SetBullet(true);
        //body->SetFixedRotation(true);
        
        // Define another box shape for our dynamic body.
        b2CircleShape *circleShape = new b2CircleShape();
        circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO);
        
        // Define the dynamic body fixture.
        b2FixtureDef fixtureDef;
        fixtureDef.shape = circleShape;
        fixtureDef.density = 50.0f;
        fixtureDef.friction = 0.1f;
        fixtureDef.restitution = 0.0f;
        fixtureDef.filter.categoryBits = CATEGORY_BULLET;
        fixtureDef.filter.maskBits = MASK_BULLET;
        
        delete circleShape;
        circleShape = NULL;
        
        body->CreateFixture(&fixtureDef);
        body->SetUserData(this);
        
        return true;
    }
    return false;
}