//
//  LDAppDelegate.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#include "AppDelegate.h"

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "ChangeLevel.h"

USING_NS_CC;
using namespace CocosDenshion;

typedef struct tagResource
{
    CCSize sizeInPixel;
    CCSize sizeDesign;
} Resource;

static Resource resPhone            =  { CCSizeMake(480, 320),   CCSizeMake(480, 320)  };//"sd"
static Resource resPhoneRetina35    =  { CCSizeMake(960, 640),   CCSizeMake(480, 320)  };//"hd"
static Resource resPhoneRetina40    =  { CCSizeMake(1136, 640),  CCSizeMake(568, 320)  };//"hd" }
static Resource resTable            =  { CCSizeMake(1024, 768),  CCSizeMake(1024, 768) };//"hd"
static Resource resTableRetina      =  { CCSizeMake(2048, 1536), CCSizeMake(1024, 768) };//"2hd"

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

    // turn on display FPS
    pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    /*****************************************************
     *  Setting specific resolution and scale starts here
     *****************************************************/
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
    CCSize frameSize = pEGLView->getFrameSize();
    

    std::vector<std::string> searchPaths;
    Resource actualResource;
    float actualWidth = MAX(frameSize.width, frameSize.height);
    if (actualWidth > resPhoneRetina40.sizeInPixel.width) {
        actualResource = resTableRetina;
        searchPaths.push_back("2hd");
    } else if (actualWidth > resTable.sizeInPixel.width) {
        actualResource = resPhoneRetina40;
        searchPaths.push_back("hd");
    } else if (actualWidth > resPhoneRetina35.sizeInPixel.width) {
        actualResource = resTable;
        searchPaths.push_back("hd");
    } else if (actualWidth > resPhone.sizeInPixel.width) {
        actualResource = resPhoneRetina35;
        searchPaths.push_back("hd");
    } else {
        actualResource = resPhone;
        searchPaths.push_back("sd");
    }

    CCFileUtils::sharedFileUtils()->setSearchPaths(searchPaths);
    
    pDirector->setContentScaleFactor(actualResource.sizeInPixel.height / actualResource.sizeDesign.height);
    pEGLView->setDesignResolutionSize(actualResource.sizeDesign.width, actualResource.sizeDesign.height, kResolutionFixedWidth);
    /*****************************************************
     *  Resolutions are handled automatically now, grats!
     *****************************************************/
    
    //SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("sounds/background.mp3");
    SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/background.mp3");
    SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/powerUP.wav");
    SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/shoot.wav");
    SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/addTime.wav");
    SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/Hit_Hurt.wav");
    // create a scene. it's an autorelease object
    CCScene *pScene = ChangeLevel::scene();

    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    SimpleAudioEngine::sharedEngine()->pauseAllEffects();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->startAnimation();
    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}
