//
//  GameLayer.cpp
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#include "GameLayer.h"
#include "MapHelper.h"
#include "Constants.h"
#include "Wall.h"
#include "Clock.h"
#include "Enemy.h"
#include "WinLayer.h"
#include "GameManager.h"
#include "SnowBonus.h"

#include "SimpleAudioEngine.h"
using namespace CocosDenshion;

#define ifDynamicCast(__type__,__var__,__varName__) \
__type__ __varName__ = dynamic_cast<__type__>(__var__); \
if( __varName__ )

#pragma mark -
#pragma mark init and deaaloc metods

CCScene* GameLayer::scene()
{
    CCScene *scene = CCScene::create();

    CCLayer* layer = new GameLayer();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}

GameLayer::GameLayer()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("GameObjects/GameObjects.plist");
    m_gameBatchNode = CCSpriteBatchNode::create("GameObjects/GameObjects.png");
    playerTime = START_PLAYER_TIME;
    gameTime = 0;
    isStopTime = false;
    setTouchEnabled(true);
    
    m_walls = new CCArray();
    m_walls->initWithCapacity(1);
    
    initPhysics();
    addMap();
    
    m_map->addChild(m_gameBatchNode, 1);
    
    m_hudLayer = new HUDLayer();
    m_hudLayer->autorelease();
    addChild(m_hudLayer, 10);
    
    m_whileLayer = CCLayerColor::create(ccc4(255, 255, 255, 255));
    m_whileLayer->setVisible(false);
    addChild(m_whileLayer,11);

    m_player->playerParticle = CCParticleSystemQuad::create("particles/playerParticle.plist");
    m_player->playerParticle->setPositionType(kCCPositionTypeRelative);
    m_player->playerParticle->setScale(m_player->getScale());
    m_player->playerParticle->setPosition(m_player->getPosition());
    m_map->addChild(m_player->playerParticle, 0);
    
    timeOfChange = 10;
    if (GameManager::sharedGameManager()->currentLevel == 1)
    {
        timeOfChange = 3;
    }
    //SimpleAudioEngine::sharedEngine()->playEffect("sounds/background.mp3",-1);
    scheduleUpdate();
}

GameLayer::~GameLayer()
{
    m_walls->removeAllObjects();
    delete m_walls;
}

void GameLayer::initPhysics()
{
    CCSize s = CCDirector::sharedDirector()->getWinSize();
    b2Vec2 gravity;
    gravity.Set(0.0f, 0.0f);
    m_world = new b2World(gravity);
    m_world->SetAllowSleeping(true);
    m_world->SetContinuousPhysics(true);
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    
    // Define the ground body.
    b2BodyDef groundBodyDef;
    groundBodyDef.position.Set(0, 0); // bottom-left corner
    
    b2Body* groundBody = m_world->CreateBody(&groundBodyDef);
    // Define the ground box shape.
    b2EdgeShape groundBox;
    
    // bottom
    groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
    groundBody->CreateFixture(&groundBox,0);
    
    // Create contact listener
    m_contactListener = new MyContactListener();
    m_world->SetContactListener(m_contactListener);
}
#pragma mark -
#pragma mark add Objects
void GameLayer::addMap()
{
    int level = GameManager::sharedGameManager()->currentLevel;
    CCString *nameMap = CCString::createWithFormat("LevelMap/Level_%d.tmx", level);
    m_map = CCTMXTiledMap::create(nameMap->getCString());
    parseTileMap();
    addChild(m_map);
}

#pragma mark -
#pragma mark tick
void GameLayer::update(float dt)
{
    if (!isStopTime)
    {
        playerTime -= dt;
        if (playerTime < 0) {
            gameOver();
        }else{
            
            m_hudLayer->updateTime(playerTime);
            
        }
        
        gameTime += dt;
        if (gameTime > timeOfChange)
        {
            changeGame();
            gameTime = 0;
        }
        int intTime = (int)gameTime;
        m_playerLabel->setString(CCString::createWithFormat("%d",intTime)->getCString());
    }

    
    //It is recommended that a fixed time step is used with Box2D for stability
    //of the simulation, however, we are using a variable time step here.
    //You need to make an informed choice, the following URL is useful
    //http://gafferongames.com/game-physics/fix-your-timestep/
    
    int velocityIterations = 8;
    int positionIterations = 1;
    
    // Instruct the world to perform a single step of simulation. It is
    // generally best to keep the time step and iterations fixed.
    m_world->Step(dt, velocityIterations, positionIterations);
    
    //Iterate over the bodies in the physics world
    for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            //Synchronize the AtlasSprites position and rotation with the corresponding body
            CCSprite* myActor = (CCSprite*)b->GetUserData();
            myActor->setPosition( CCPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO) );
            myActor->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()) );
            if (myActor->getTag() == NeedDestroyBody)
            {
                myActor->removeFromParentAndCleanup(true);
                m_world->DestroyBody(b);
            }
        }
    }
    ////Contacts
    std::vector<MyContact>::iterator pos;
    for(pos = m_contactListener->_contacts.begin(); pos != m_contactListener->_contacts.end(); ++pos) {
        MyContact contact = *pos;
        
        b2Body *bodyA = contact.fixtureA->GetBody();
        b2Body *bodyB = contact.fixtureB->GetBody();
    
        if (bodyA->GetUserData() != NULL && bodyB->GetUserData() != NULL) {
            CCSprite *spriteA = (CCSprite *) bodyA->GetUserData();
            CCSprite *spriteB = (CCSprite *) bodyB->GetUserData();
            
            if (spriteA->getTag() == ObjectTypeBullet)
            {
                if (spriteB->getTag() == ObjectTypePlayer)
                {
                    m_player->frize();
                }
                spriteA->setTag(NeedDestroyBody);
            }
            if (spriteB->getTag() == ObjectTypeBullet)
            {
                if (spriteA->getTag() == ObjectTypePlayer)
                {
                    m_player->frize();
                }
                spriteB->setTag(NeedDestroyBody);

            }
            
//with player
            b2Body *playerBody;
            b2Body *objectBody;
            if (spriteA->getTag() == ObjectTypePlayer)
            {
                playerBody = bodyA;
                objectBody = bodyB;
            }else{
                playerBody = bodyB;
                objectBody = bodyA;
            }
            
            CCSprite *object = (CCSprite*) objectBody->GetUserData();
            
            if (object->getTag() == ObjectTypeClock) {
                object->setTag(NeedDestroyBody);
                playerTime += 10;
                SimpleAudioEngine::sharedEngine()->playEffect("sounds/addTime.wav");
            }else if (object->getTag() == ObjectTypeFinishLine){
                gameWin();
            }else if (object->getTag() == ObjectTypeSnowBonus){
                SimpleAudioEngine::sharedEngine()->playEffect("sounds/addTime.wav");
                stopTime();
                object->setTag(NeedDestroyBody);
            }
//            else if (object->getTag() == ObjectTypeEnemy){
//                SimpleAudioEngine::sharedEngine()->playEffect("sounds/Hit_Hurt.wav");
//            }
/////
        }
    }
    //////
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    CCPoint centerOfScreen = ccp(screenSize.width / 2, screenSize.height /2);
    m_map->setPosition(ccpAdd(ccpMult(m_player->getPosition(), -1), centerOfScreen));
    
    float angle = m_player->body->GetAngularVelocity();
    angle = angle * 0.95;
    m_player->body->SetAngularVelocity(angle);
    
    m_playerLabel->setPosition(ccpAdd(m_player->getPosition(), ccp(0.0, m_player->boundingBox().size.height/2 )));
    m_player->playerParticle->setPosition(m_player->getPosition());
}

#pragma mark -
#pragma mark other

void GameLayer::stopTime()
{
    m_hudLayer->m_time->runAction(CCTintTo::create(1.0, 123, 184, 170));
    m_playerLabel->runAction(CCTintTo::create(1.0, 123, 184, 170));

    isStopTime = true;
    CCDelayTime *delay = CCDelayTime::create(FRIZE_TIME);
    CCCallFunc *changed = CCCallFunc::create(this, callfunc_selector(GameLayer::playTime));
    CCSequence *seq = CCSequence::createWithTwoActions(delay, changed);
    
    runAction(seq);
}

void GameLayer::playTime()
{
    m_hudLayer->m_time->runAction(CCTintTo::create(0.5, 255, 255, 255));
    m_playerLabel->runAction(CCTintTo::create(0.5, 255, 255, 255));
    isStopTime = false;
}

void GameLayer::changeGame()
{
    m_whileLayer->setOpacity(0.0f);
    m_whileLayer->setVisible(true);
    
    CCFadeOut *fade = CCFadeOut::create(0.4f);
    CCCallFunc *changed = CCCallFunc::create(this, callfunc_selector(GameLayer::changed));
    CCSequence *seq = CCSequence::createWithTwoActions(fade, changed);
    m_whileLayer->runAction(seq);
}

void GameLayer::changed()
{
    m_player->change();
}

void GameLayer::parseTileMap()
{
    CCArray * groups = m_map->getObjectGroups();
    for( int i = 0; i < groups->count(); i++ )
    {
        ifDynamicCast(CCTMXObjectGroup*, groups->objectAtIndex(i), group)
        {
            auto objects = group->getObjects();
            for (int j = 0; j < objects->count(); j++)
            {
                ifDynamicCast(CCDictionary*, objects->objectAtIndex(j), properties)
                {
                    addObject(properties);
                }
            }
        }
    }
}

void GameLayer::addObject(CCDictionary *properties)
{
    const CCString * type = properties->valueForKey("type");
    if (type->compare("spawnPlayer") == 0)
    {
        CCPoint pos = MapHelper::positionWithProperties(properties);
        CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
        CCPoint centerOfScreen = ccp(screenSize.width / 2, screenSize.height /2);
        m_map->setPosition(ccpAdd(ccpMult(pos, -1), centerOfScreen));
        
        m_player = new Player();
        m_player->init(m_world, properties);
        m_player->autorelease();
        m_gameBatchNode->addChild(m_player, 1);
        
        m_playerLabel = CCLabelBMFont::create("10", "Fonts/TimeFont.fnt");
        m_playerLabel->setPosition(ccpAdd(m_player->getPosition(), ccp(0.0, 20)));
        m_playerLabel->setAnchorPoint(ccp(0.5, 0.0));
        m_playerLabel->setScale(0.5);
        m_map->addChild(m_playerLabel,3);
        
    }else if(type->compare("wall") == 0){
        Wall *wall = new Wall();
        wall->init(m_world, properties);
        m_walls->addObject(wall);
        wall->autorelease();
        addChild(wall);
        //CCLog("points->count() =%d",wall->points->count());
    }else if (type->compare("spawnClock") == 0){
        Clock *clock = new Clock();
        clock->init(m_world, properties);
        clock->autorelease();
        m_gameBatchNode->addChild(clock);
    }else if (type->compare("spawnEnemy") == 0){
        Enemy *enemy = new Enemy();
        enemy->init(m_world, properties, 0);
        enemy->autorelease();
        enemy->gameGelegate = this;
        m_gameBatchNode->addChild(enemy);
    }else if (type->compare("spawnShooter") == 0){
        Enemy *enemy = new Enemy();
        enemy->init(m_world, properties, 1);
        enemy->autorelease();
        enemy->gameGelegate = this;
        m_gameBatchNode->addChild(enemy);
    }else if (type->compare("finishLine") == 0){
        Wall *wall = new Wall();
        wall->init(m_world, properties);
        wall->setTag(ObjectTypeFinishLine);
        wall->autorelease();
        addChild(wall);
    }else if (type->compare("snowBonus") == 0){
        SnowBonus *snowBonus = new SnowBonus();
        snowBonus->init(m_world, properties);
        snowBonus->autorelease();
        m_gameBatchNode->addChild(snowBonus);
    
    }
}

void GameLayer::gameOver()
{
    unscheduleUpdate();
    CCDirector::sharedDirector()->replaceScene(GameLayer::scene());
}

void GameLayer::gameWin()
{
    unscheduleUpdate();
    CCDirector::sharedDirector()->replaceScene(WinLayer::scene());
}

#pragma mark -
#pragma mark touch metods
void GameLayer::ccTouchesBegan(CCSet* touches, CCEvent* event)
{
    m_player->setSlowdown(false);
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint pos = ccpSub(location, m_map->getPosition());
 
        CCPoint playerPos = m_player->getPosition();
 
        CCPoint direction = ccpNormalize(ccpSub(pos, m_player->getPosition()));
        m_player->moveToDirection(direction);
    }
}

void GameLayer::ccTouchesMoved(CCSet* touches, CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint pos = ccpSub(location, m_map->getPosition());
        
        CCPoint playerPos = m_player->getPosition();
        
        CCPoint direction = ccpNormalize(ccpSub(pos, m_player->getPosition()));
        m_player->moveToDirection(direction);
    }
}

void GameLayer::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint pos = ccpSub(location, m_map->getPosition());
        
        CCPoint playerPos = m_player->getPosition();
        
        CCPoint direction = ccpNormalize(ccpSub(pos, m_player->getPosition()));
        m_player->moveToDirection(direction);
        m_player->setSlowdown(true);
    }
}

#pragma mark -
#pragma mark GameDelegate

CCArray* GameLayer::getWalls()
{
    return m_walls;
}

Player* GameLayer::getPlayer()
{
    return m_player;
}