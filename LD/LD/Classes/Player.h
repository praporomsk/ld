//
//  Player.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__Player__
#define __LD__Player__

#include <iostream>
#include "cocos2d.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class Player : public CCSprite
{
    
public:
    ~Player();
    Player();
    
    bool init(b2World* world, CCDictionary *properties);
    void moveToDirection(CCPoint direction);
    void update(float dt);
    b2Body *body;
   
    void setSlowdown(bool slowdown);

    void change();
    void changed();
    void reCreateBody();
    void frize();
    void unFrize();
    
    bool isFrieze;
    CCParticleSystemQuad *playerParticle;
private:
    
    bool m_slowdown;
    b2World *m_world;
};


#endif /* defined(__LD__Player__) */
