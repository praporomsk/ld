//
//  HUDLayer.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__HUDLayer__
#define __LD__HUDLayer__

#include "cocos2d.h"
USING_NS_CC;

class HUDLayer : public CCLayer {
public:
    ~HUDLayer();
    HUDLayer();
    
    void exit();
    void updateTime(float time);
    CCLabelBMFont *m_time;
};
#endif /* defined(__LD__HUDLayer__) */
