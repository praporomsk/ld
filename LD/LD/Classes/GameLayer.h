//
//  GameLayer.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__GameLayer__
#define __LD__GameLayer__

#include <iostream>
#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Player.h"
#include "MyContactListener.h"
#include "GameDelegate.h"
#include "HUDLayer.h"

USING_NS_CC;

class GameLayer : public CCLayer, public GameDelegate {
public:
    ~GameLayer();
    GameLayer();
    
    static CCScene* scene();
    void addMap();
    void parseTileMap();
    void addObject(CCDictionary *properties);
    void initPhysics();
    void update(float dt);
    void gameOver();
    void gameWin();
    
    void changeGame();
    void changed();
    CCArray* getWalls();
    Player* getPlayer();
    
    void stopTime();
    void playTime();
    
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
private:

    bool isStopTime;
    int timeOfChange;
    float playerTime;
    float gameTime;
    CCLayerColor *m_whileLayer;
    HUDLayer *m_hudLayer;
    CCArray *m_walls;
    b2World* m_world;
    CCTMXTiledMap *m_map;
    CCSpriteBatchNode *m_gameBatchNode;
    Player *m_player;
    CCLabelBMFont *m_playerLabel;
    MyContactListener *m_contactListener;
};
#endif /* defined(__LD__GameLayer__) */
