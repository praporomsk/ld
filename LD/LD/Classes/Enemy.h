//
//  Enemy.h
//  LD
//
//  Created by Roman on 8/24/13.
//
//

#ifndef __LD__Enemy__
#define __LD__Enemy__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "GameDelegate.h"

USING_NS_CC;

class Enemy : public CCSprite
{
    
public:
    ~Enemy();
    Enemy();
    
    bool init(b2World* world, CCDictionary *properties, int type);
    int m_type;
    void updateAI();
    bool isSeePlayer();
    void playUpdate();
    bool intersect (CCPoint a, CCPoint b, CCPoint c, CCPoint d);
    b2Body *body;
    GameDelegate *gameGelegate;
private:
    b2World *m_world;
};
#endif /* defined(__LD__Enemy__) */
